\beamer@sectionintoc {1}{Introduction to formal verification}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{SMT calculus}{10}{0}{1}
\beamer@subsectionintoc {1}{2}{Abstract interpretation and propagation}{16}{0}{1}
\beamer@sectionintoc {2}{Formal verification of deep neural networks}{20}{0}{2}
\beamer@subsectionintoc {2}{1}{SMT approaches}{33}{0}{2}
\beamer@subsectionintoc {2}{2}{Abstract Interpretation approaches}{37}{0}{2}
\beamer@sectionintoc {3}{Practical session}{68}{0}{3}
\beamer@subsectionintoc {3}{1}{Properties formulation}{72}{0}{3}
