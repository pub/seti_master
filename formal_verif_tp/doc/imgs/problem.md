## Problem definition

Let A be a Guardian, and B a Threat. The goal for the
Guardian is to send an ALARM when the Threat arrives
too near.

The Guardian has access to the following data:
* $||\vec{d}||$, the norm of the vector going from B to A
* $||\vec{v}||$, the norm of the speed vector of B
* $\frac{\vec{d}\cdot\vec{v}}{|\vec{d}\cdot\vec{v}|}$, the
  normalized scalar product between $\vec{d}$ and $\vec{v}$

There are three main "zones":
1. a "safe" zone: when B is in this zone, it is not
   considered a threat and thus no ALARM is issued.
   When $||\vec{d}||>\delta_2$,
   B is considered in the safe zone.
2. a "suspicious" zone: when B is in this zone, if the norm
   of its speed vector is above a certain threshold $\alpha$
   and the normalized scalar product is above a certain value
   $\beta$ (denoting that the Threat is moving fast towards
   the Guardian) then a ALARM should be issued. Else, no
   ALARM is issued. When $||\vec{d}||>\delta_1$ and
   $||\vec{d}||<\delta_2$, B is in the suspicious zone.
3. a "danger" zone: when B is in this zone, a ALARM is issued
   no matter what. When $||\vec{d}||<\delta_1$, B is in the
   danger zone.

There is a fourth zone, a "blank zone", delimited by all the
area of distance $\delta_0$ of A. This blank zone can be
used to model sensors inaccuracy or an vulnerability in the
system (for instance, if the speed vector of B's norm is
too high, B can go from the safe zone to the danger zone
immediately).

## Rationale

The number of variables is low (three), we could even reduce
it to two by forcing the neural network to learn the
normalized scalar product itself, but it may lead to
a deeper architecture which would be more difficult to
verify.

The learned function is non-linear but quite simple:
"if B is inside the danger zone, or if B is inside the
suspicious and B is going fast towards A, then ALARM, else
NO ALARM". A neural network with two or three hidden layers
with a small number of hidden units should be able to
learn it on a sufficient number of examples. If it is
"too easy", we can further add conditions to send an ALARM,
add other zones to increase the number of non linearities,
or change the input variables to include the coordinates of
B.

This simple problem already leads to the following
properties to check

## Problem formulations
1. B in the danger zone always leads to ALARM
1. B in suspicious zone and not moving towards A always
   leads to NO ALARM
1. B in suspicious zone and moving towards A and at speed
   over $\beta$ always leads to ALARM
1. B in suspicious zone and moving towards A and at speed
   below $\beta$ does not lead to ALARM
1. B in safe zone never leads to ALARM
1. Consistent classification: for all situations where B
   leads to ALARM, another situation where B is nearer of
   A will also lead to ALARM
   
