{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Formal verification of deep neural networks: a tutorial\n",
    "\n",
    "This notebook is included in the online tutorial given during the INRIA-DFKI IDESSAI summer school _Formal verification of deep neural networks: theory and practice_.\n",
    "\n",
    "The aim of this notebook is to give a glimpse on the practical side of Formal Verification for Deep Neural Networks.\n",
    "We provided a toy problem representative of current challenges in neural network verification. We also trained a deep neural network to answer this problem: it will be available under the standard ONNX binary representation.\n",
    "\n",
    "The goal of the tutorial for the participants is to formally verify that the neural network is _safe_ , using the bundled tools.\n",
    "\n",
    "The tutorial material was written by Julien Girard-Satabin (CEA LIST/INRIA), Zakaria Chihani (CEA LIST) and Guillaume Charpiat (INRIA). Augustin Lemesles (CEA LIST) provided a clean implementation of PyRAT as well as a converter for Marabou input format. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem definition\n",
    "\n",
    "This toy problem is inspired by the Airborne Collision Avoidance System for Unmanned vehicles (ACAS-Xu) specification and threat model. \n",
    "For an introduction of the ACAS-Xu benchmark, see _An Introduction to ACAS Xuand the Challenges Ahead_ , Manfredi G., Jestin Y.\n",
    "\n",
    "Formerly, programs such as ACAS were implemented as a lookup table, which was costly in memory. Neural network becoming more and more efficient, industrials considered using them as a possible replacement. Since we are dealing with critical systems, software safety is a major concern.\n",
    "\n",
    "In _Reluplex: An Efficient SMT Solver for Verifying Deep Neural Networks_ , Katz et al. provided a neural network implementation of a ACAS-Xu system, as well as a tooling to formally verify several safety properties. It was then adopted as a common benchmark in the literature.\n",
    "\n",
    "The neural network proposed by Katz had 6 hidden layers with 300 ReLU nodes. With this size, some verification tools will take at least several minutes to answer, which is not practical in the timeframe of this tutorial. We thus decided to scale down the problem and the network to allow a wider window of experimentation. \n",
    "\n",
    "![problem formulation](doc/imgs/problem_small.png)\n",
    "\n",
    "Let A be a Guardian, and B a Threat.\n",
    "The goal for the Guardian is to send an\n",
    "ALARM when the Threat arrives too near.\n",
    "The Guardian has access to the following data:\n",
    "* the norm of the vector going from B to A $||\\vec{d}||$\n",
    "* the norm of the speed vector of B $||\\vec{v} ||$\n",
    "* the angle $\\theta$ between $\\vec{d}$ and $\\vec{v}$\n",
    "\n",
    "All values are normalized in $\\left[0,1\\right]$, the angle is not oriented\n",
    "\n",
    "There are three main ”zones”:\n",
    "1. a ”safe” zone: when B is in this zone, it is not considered a threat\n",
    "  * for any $||\\vec{d}|| > \\delta_2$,\n",
    "    B is considered in the safe zone, thus no ALARM is issued.\n",
    "1. a ”suspicious” zone: when B is in this zone,\n",
    "   if $||\\vec{v}|| > \\alpha$ and $\\theta < \\beta$\n",
    "   (denoting that the Threat is moving fast towards the Guardian)\n",
    "   then a ALARM should be issued. Else, no ALARM is issued.\n",
    "  * When $\\delta_2 > ||\\vec{d}||> \\delta_1$, B is in the suspicious zone.\n",
    "1. a ”danger” zone: when B is in this zone, a ALARM is issued no matter\n",
    "   what.\n",
    "  * When $||\\vec{d}|| < \\delta_1$, B is in the danger zone.\n",
    "\n",
    "  \n",
    "### Solving this problem with a neural network\n",
    "A neural network was pre-trained to solve this task (all files used to this end are available in the repository). \n",
    "It has 5 fully connected layers, the first layer takes 3 inputs and the last layer has 1 output. There are four hidden layers: first and second hidden layers are of size 10, the third is size 5 and the fourth is size 2. We used ReLUs as activation functions. \n",
    "\n",
    "The network was trained to output a positive value if there is an alarm, and a negative value if there is no alarm. For a detailed summary of hyperparameters, you may check the defaults in `train.py`. It achieved 99.9% accuracy on the test set, with a total training set of 100000 samples.\n",
    "\n",
    "\n",
    "\n",
    "![title](doc/imgs/network.png)\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem solving\n",
    "\n",
    "The trained network is in the repository, under the filename `network.onnx`. Your goal is to convert it to the SMTLIB output format, learn how to write a safety property and launch different tools on the network.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Various imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cupy module not found, GPU will be disabled\n"
     ]
    }
   ],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "from formula_lang import *\n",
    "from pyrat_api import launch_pyrat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Visualization \n",
    "\n",
    "You can first visualize the network answer on the output space by sampling inputs,\n",
    "using the function below (**careful, it may take time if you input a big number of samples!**).\n",
    "\n",
    "`sample2d` is faster but sample only on a 2d slice, `sample3d` gives a full representation of the output space.\n",
    "\n",
    "Blue color denotes no alert, red color denotes an alert.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4747b5809a314e208feace92d23bd33d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f4007e56daf145dea17f49622f5a46cd",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "from visualize_outputs import sample2d, sample3d, plot2d, plot3d\n",
    "from onnx2pytorch import convert\n",
    "\n",
    "%matplotlib widget\n",
    "\n",
    "n_sample = 1000  #number of points to sample\n",
    "frozen_dim = 0    #which dimension will have a constant value for the 2d plot\n",
    "frozen_val = 0.9    #constant value to give\n",
    "model = convert(\"network.onnx\")\n",
    "dim_1, dim_2, colours = sample2d(model, n_sample, frozen_dim, frozen_val)\n",
    "plot2d(dim_1, dim_2, colours)\n",
    "dim_1, dim_2, dim_3, colours = sample3d(model, n_sample)\n",
    "plot3d(dim_1, dim_2, dim_3, colours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conversion from ONNX to SMTLIB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def call_isaieh(fpath):\n",
    "    \"\"\"Convert an ONNX network at fpath to a SMT formula describing;\n",
    "    the control flow. The output will be called fpath_QF_NRA.smt2\"\"\"\n",
    "    os.environ[\"ISAIEH_INPUT\"] = fpath\n",
    "    !./isaieh.exe -theory=QF_NRA $ISAIEH_INPUT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "zsh:1: aucun fichier ou dossier de ce type: ./isaieh.exe\n"
     ]
    }
   ],
   "source": [
    "call_isaieh('network.onnx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Description of safety properties, example\n",
    "\n",
    "Let us say we would like to check the following property:\n",
    "\"no input above a distance .95 output an alarm\". If our neural network correctly follows our specification, it should respect this property (since the distance $||\\vec{AB}||$ is above 0.7, no alarm should be issued).\n",
    "\n",
    "The basics for formulating properties to our various tools is the following:\n",
    "\n",
    "1. Write constraints on inputs as defined in the specification\n",
    "1. State the contraint on outputs you want to check\n",
    "1. For tools using SMT, write down the negation of this constraint\n",
    "   (remember in the course section,\n",
    "   VALID(F) is equivalent to ¬SAT(¬F))\n",
    "\n",
    "For this tutorial, we wrote a simple API to make easier the writing of properties to check. This API is detailed inside `formula_lang.py` in the repository.\n",
    "This API allow to define linear constraints on symbolic variables and real values.\n",
    "* To define a new variable, use the constructor `Var(x)`, where `x` must be a string\n",
    "* To define a new real number to be used in the constraint, use `Real(r)` where `r` is a python number\n",
    "* A constraint is a linear inequality between two variables or reals. To create a new constraint, use\n",
    "  `constr = Constr(var1,bop,var2)` where `bop` is either `'>='` or `<`.\n",
    "* You can create multiple constraints\n",
    "* Finally, once you are satisfied, you can add your constraints to a formula. A formula is a conjunction of constraints\n",
    "* `f = Formula()` creates an empty constraint, and `f.add(constr)` add the constraint `constr` to the formula.\n",
    "  `f.add(c1)` followed by `f.add(c2)` is equivalent to adding a conjunction of `c1` and `c2`\n",
    "\n",
    "\n",
    "Here is how to use it:\n",
    "#### Variables creations\n",
    "1. create a new variable with `var = Var(str)`; `str` should be    \n",
    "  either `'x0'`, `'x1'`, `'x2'` or `'y0'`, respectively the first, \n",
    "  second and third input and only output. For convenience, they \n",
    "  are already defined when executing the cell below as\n",
    "  `distance`, `speed`, `angle` and `output`\n",
    "1. create a new real value with `real = Real(r)` where `r`\n",
    "  can be an integer or a float (all variables will be converted\n",
    "  as real values). For instance, `real = Real(0.95)`\n",
    "#### Creating constraints and adding them to a formula\n",
    "1. create a new constraint between a variable `var` and a\n",
    "  real value `real` with `constr = Constr(var,bop,real)` where\n",
    "  `bop` is either `'>='` or `'<'`. For instance, `constr = Constr(distance,'>=',real)`\n",
    "1. create a new empty formula with `f = Formula()`\n",
    "1. add a constraint `constr` to a formula `f`\n",
    "  with `f.add(constr)`. You may also add a conjunction of constraints with\n",
    "    `f.add((constr1,'and',constr2))` (note the tuple input).\n",
    "    If you target SMTLIB, you may also add disjunctions with\n",
    "    `f.add((constr1,'or',constr2))`, disjunctions will not be\n",
    "    converted to Marabou however.\n",
    "#### Printing and saving to disk\n",
    "1. print a formula `f` with `print(f)`\n",
    "1. write down a formula `f` to SMTLIB2 format at destination `dest`\n",
    "  with `f.write_smtlib(dest)`; similarly for Marabou format,\n",
    "  use `f.write_marabou(dest)` \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Wrote SMT formula in file formula.smt2\n",
      "Warning: Marabou does not support strict inequality,                     using Lower or Equal ( <= ) instead\n",
      "Warning: Marabou does not support strict inequality,                     using Lower or Equal ( <= ) instead\n",
      "Warning: Marabou does not support strict inequality,                     using Lower or Equal ( <= ) instead\n",
      "Warning: Marabou does not support strict inequality,                     using Lower or Equal ( <= ) instead\n",
      "Wrote marabou formula in file formula.marabou\n"
     ]
    }
   ],
   "source": [
    "distance = Var('x0')\n",
    "speed = Var('x1')\n",
    "angle = Var('x2')\n",
    "output = Var('y0')\n",
    "one = Real(1)\n",
    "real = Real(0.95)\n",
    "zero = Real(0)\n",
    "constrs = []\n",
    "constrs.append(Constr(distance,'>=',real))\n",
    "constrs.append(Constr(speed,'>=',real))\n",
    "constrs.append(Constr(angle,'>=',real))\n",
    "constrs.append(Constr(distance,'<=',one))\n",
    "constrs.append(Constr(speed,'<=',one))\n",
    "constrs.append(Constr(angle,'<=',one))\n",
    "constrs.append(Constr(output,'>=',zero))\n",
    "constrs.append(Constr(output,'<=',one))\n",
    "formula = Formula()\n",
    "for c in constrs:\n",
    "    formula.add(c)\n",
    "formula.write_smtlib()\n",
    "formula.write_marabou()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Launch solvers and recover results\n",
    "\n",
    "We use three tools in this tutorial:\n",
    "\n",
    "1. Z3, a theorem prover from Microsoft Research [https://github.com/Z3Prover/z3](https://github.com/Z3Prover/z3), used as a state-of-the-art SMT solver; however it does not have any particular heuristics to work on neural networks \n",
    "2. PyRAT, a tool internally developped at the lab, that leverages abstract interpretation to verify reachability properties on neural networks. The source is currently not available, if you want to access it just send us an email\n",
    "3. Marabou, a solver tailored for neural network verification: it uses a specialized Simplex algorithm and merges relevant neurons. See [the paper](https://arxiv.org/abs/1910.14574) for more details\n",
    "\n",
    "Below are some utilities functions to launch those different tools.\n",
    "You will notice that PyRAT only perform a \"reachability analysis\" (given the input range, what is the possible output range)? Marabou does not deal with disjunction of clauses ($a< x1 <b \\vee c < x1 <d$) , so you will need to formulate the two clauses in separate properties (one with $a< x1 <b$, one with $c < x1 <d$).\n",
    "\n",
    "It is partly due to implementation constraints, and on such simple problem this should not be a limitation. But the set of expressible properties is different between abstract interpretation and SAT/SMT calculus.\n",
    "\n",
    "Here is a recap about the tools we will be using:\n",
    "\n",
    "| | Z3 | Marabou | PyRAT \t|\n",
    "|----------------------------------\t|--------------------------------\t|--------------------------------------\t|-------------------------\t|\n",
    "| input format                     \t| SMTLIB (with formula.write_smt()) \t| Specific (with formula.write_marabou()) \t| min/max intervals       \t|\n",
    "| technology                       \t| SMT                            \t| SMT / overapproximation              \t| abstract interpretation \t|\n",
    "| specialized  for neural networks \t| no                             \t| yes                                  \t| yes                     \t|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def launch_z3(control_flow,constraints):\n",
    "    \"\"\"Launch z3 on the SMT problem\n",
    "    composed of the concatenation of a control flow and constraints, both written in SMTLIB2\"\"\"\n",
    "    os.environ[\"Z3_CONTROL_FLOW\"] = control_flow\n",
    "    os.environ[\"Z3_CONSTRAINTS\"] = constraints\n",
    "    !cat $Z3_CONTROL_FLOW $Z3_CONSTRAINTS > z3_input\n",
    "    !z3 z3_input\n",
    "\n",
    "def launch_marabou(constraints):\n",
    "    \"\"\"Launch marabou on the verification problem:\n",
    "    network is a .nnet description of the network (provided here\n",
    "    for simplicity) and constraints is a property file you wrote\"\"\"\n",
    "    os.environ[\"MARABOU_PROPERTY\"] = constraints\n",
    "    !./marabou.elf 'network.nnet' $MARABOU_PROPERTY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To launch Z3, provide the filepath of the network's control flow in SMTLIB as well as the filepath of the\n",
    "formula you generated. \n",
    "A SAT result will be followed by an instanciation of the input and outputs that satisfies your property. An UNSAT result will throw some errors, because we ask to get an instanciation fo variables where there exist none. It's okay, the important part is that the verification problem you gave to Z3 is UNSAT. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "cat: network_QF_NRA.smt2: Aucun fichier ou dossier de ce type\n",
      "(error \"line 1 column 13: unknown constant CELL_actual_input_0_0_0\")\n",
      "(error \"line 2 column 12: unknown constant CELL_actual_input_0_0_1\")\n",
      "(error \"line 3 column 12: unknown constant CELL_actual_input_0_0_2\")\n",
      "(error \"line 4 column 12: unknown constant CELL_actual_input_0_0_0\")\n",
      "(error \"line 5 column 12: unknown constant CELL_actual_input_0_0_1\")\n",
      "(error \"line 6 column 12: unknown constant CELL_actual_input_0_0_2\")\n",
      "(error \"line 7 column 12: unknown constant CELL_actual_output_0_0_0\")\n",
      "(error \"line 8 column 12: unknown constant CELL_actual_output_0_0_0\")\n",
      "sat\n",
      "(error \"line 10 column 12: unknown constant CELL_actual_input_0_0_0\")\n",
      "(error \"line 11 column 12: unknown constant CELL_actual_input_0_0_1\")\n",
      "(error \"line 12 column 12: unknown constant CELL_actual_input_0_0_2\")\n",
      "(error \"line 13 column 12: unknown constant CELL_actual_output_0_0_0\")\n"
     ]
    }
   ],
   "source": [
    "launch_z3('network_QF_NRA.smt2','formula.smt2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly for Marabou, provide the filepath of the property you generated on marabou format. There is no need to provide the input network here, as there is already a `network.nnet` representation inside the repository. The output of Marabou is more verbose dans Z3, the important part lies at the end: SAT or UNSAT. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Network: network.nnet\n",
      "Number of layers: 6. Input layer size: 3. Output layer size: 1. Number of ReLUs: 27\n",
      "Total number of variables: 58\n",
      "Property: formula.marabou\n",
      "\n",
      "Engine::processInputQuery: Input query (before preprocessing): 28 equations, 58 variables\n",
      "UNSAT\n",
      "free(): invalid pointer\n"
     ]
    }
   ],
   "source": [
    "launch_marabou('formula.marabou')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyRAT API provide directly a function to launch the analysis. Remember that a _negative value_ means\n",
    "no alarm issued, while a _positive value_ means that an alarm is issued."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Result = False, Time = 0.00 s, Safe space = 0.00 %, number of analysis = 1\n"
     ]
    }
   ],
   "source": [
    "launch_pyrat(\"network.onnx\", \"formula.marabou\", [\"zonotopes\"], nb_of_splits=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discussion and going further\n",
    "\n",
    "You were able to launch solvers to verify this simple property. Notice the several points:\n",
    "* Z3 took significantly more time than the others to return a  \n",
    "  result: it comes from the suboptimal encoding of the problem\n",
    "  as well as the lack of heuristics tailored to neural network\n",
    "  verification, such as the ones we saw in the first part of the\n",
    "  tutorial. Using Z3 on more complex properties will likely hang\n",
    "  your session; don't hesitate to terminate the cell's execution\n",
    "  if it takes too much time\n",
    "* With the default input ranges provided for PyRAT, the output  \n",
    "  ranges is too wide to conclude. This is because [0,1.] is\n",
    "  quite a big range for PyRAT, and propagating a wide interval\n",
    "  leads to quite a loss in precision earlier. To cope with this \n",
    "  issue, you can use a common technique in abstract\n",
    "  interpretation, which consists on splitting the input domain\n",
    "  into subdomains. You will then need to generate subproblems\n",
    "  and check them again with PyRAT.\n",
    "\n",
    "However, checking this property is not enough to ensure the safety of our neural network. Among the possible properties you way want to check:\n",
    "* any input in the safe zone does not lead to an alarm\n",
    "* any input in the danger zone always lead to an alarm\n",
    "* input in the suspicious zone that goes fast enough in the\n",
    "  direction of the Guardian always lead to an alarm\n",
    "* for any given situation yielding an alarm, an input where `B`\n",
    "  is nearer of `A` will yield an alarm\n",
    "* adversarial example: for a given input `(x1,x2,x3)`,\n",
    "  there are missclassification a ball of radius $\\varepsilon$\n",
    "* and others!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_formula = Formula()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
