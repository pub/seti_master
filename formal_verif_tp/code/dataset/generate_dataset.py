"""
This script generates data for a classifier modelling
a Guardian, opposing a Threat. The Guardian should
output an alarm when the Threat in trespassing within a zone.
The Guardian has access to three inputs: norm of the distance
vector from Threat to Guardian, norm of speed vector of Threat, angle between
the speed vector and the distance vector in radians.

This script generates data in a csv file with the following columns,
from left to right:
NORM OF DISTANCE
NORM OF SPEED VECTOR
ANGLE
OUTPUT

Value in column OUTPUT is either 0 or 1.
"""
# Imports
from random import random as r
import numpy as np
import sys

# Constants
ALPHA = 0.5  # speed threshold for suspicious zone
BETA = 0.25  # angle (radian) threshold for suspicious zone
MAX_DIST = 1.  # maximum generated distance
MAX_SPEED = 1.  # maximum speed
MAX_ANGLE = 1.  # maximum angle (symmetry)
DELTA_1 = 0.3  # limit of danger zone
DELTA_2 = 0.7  # limit of suspicious zone
EPSILON = 0.09  # threshold of frontier distance
NUM_SAMPLING = int(sys.argv[1])


def sample_data():
    """Returns a tuple (a,b,c)
    where a stands for the norm of distance vector,
    b stands for the norm of speed vector,
    c stands for the angle.

    We consider only data that are sufficiently far away from
    the limits DELTA_1 and DELTA_2
    """
    dist = r()*MAX_SPEED
    speed = r()*MAX_DIST
    angle = r()*MAX_ANGLE
    while abs(dist - DELTA_1) < EPSILON or abs(dist - DELTA_2) < EPSILON:
        dist = r() * MAX_SPEED
    while abs(speed - ALPHA) < EPSILON:
        speed = r()*MAX_DIST
    if abs(angle - BETA) < EPSILON:
        angle = r()*MAX_ANGLE
    return (dist, speed, angle)


def is_safe(a, b, c):
    """Returns TRUE if (a,b,c) is a safe state,
    and FALSE otherwise.

    A safe state is defined by the following:
    * a > DELTA_2
    * a > DELTA_1 and a < DELTA_2
      and (b < ALPHA or c > BETA)
    An unsafe state is defined by the following:
    * a < DELTA_1
    * a > DELTA_1 and a < DELTA_2
      and (b > ALPHA and c < BETA)
    """
    return(a > DELTA_2 or (DELTA_1 < a < DELTA_2 and
                           (b < ALPHA or c > BETA)))


if __name__ == "__main__":
    FNAME = 'data_ALPHA='+str(ALPHA)+'_BETA_='+str(BETA) +\
            '_DELTA1='+str(DELTA_1) +\
            '_DELTA2=' + str(DELTA_2) + \
            '_EPSILON=' + str(EPSILON) + \
            '.npy'
    data = []
    for n in range(NUM_SAMPLING):
        (a, b, c) = sample_data()
        if is_safe(a, b, c):
            res = 0
        else:
            res = 1
        data.append([a, b, c, res])
    np.save(FNAME, np.asarray(data))
    print('Dataset generation completed')
