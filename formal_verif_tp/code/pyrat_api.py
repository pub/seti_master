from NNreader.read import readOnnx
from sound_pyrat.analyzer import Box, analyze
from sound_pyrat.split import analyze_splits

from sound_pyrat.property_parser import read_property

import numpy as np


def launch_pyrat(model_path, property_path, add_domains, nb_of_splits=-1):
    """
    Args:
        model_path: path to the model
        property_path: path to the property file
        add_domains: a python list of domains to propagate in the network from "zonotopes" or "poly"
        nb_of_splits: maximum number of splits to do on the input intervals (-1 for no split)
    """
    model = readOnnx(model_path)
    input_shape = model[0]['input_shape']
    bounds, to_verify, to_counter = read_property(property_path, input_shape)

    input_interval = Box(bounds[0], bounds[1])

    if nb_of_splits == -1:
        result = analyze(input_interval, model, additional_domains=add_domains, by_layer=False)
        print("Output bounds:\n", result.output_bounds)
    else:
        result = analyze_splits(input_interval, model, to_verify, to_counter, timeout=nb_of_splits,
                                additional_domains=add_domains, layer_per_layer=False, verbose=False)
        result.print_res()
