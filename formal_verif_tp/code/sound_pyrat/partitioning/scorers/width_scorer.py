from sound_pyrat.domains.box import Box
from sound_pyrat.partitioning.scorers.abstract_scorer import AbstractScorer
import numpy as np


class WidthScorer(AbstractScorer):
    def __init__(self):
        super().__init__()

    def score(self, input_box: Box, *kwargs):
        widths = input_box.get_widths()
        return np.argsort(widths)[::-1]

    def __name__(self):
        return "WidthScorer"
