from sound_pyrat.domains.box import Box
import numpy as np

from sound_pyrat.partitioning.scorers.abstract_scorer import AbstractScorer


class OutputInfluenceScorer(AbstractScorer):
    def __init__(self):
        super().__init__()

    def score(self, input_box: Box, model, n=40):
        samples = input_box.get_samples(n)
        res = list()
        for i in range(len(input_box)):
            x = np.tile(input_box.get_centers(), reps=(n, 1))
            x[:, i] = samples[:, i]
            y = OutputInfluenceScorer._runModel(model, x)
            upper = np.max(y, axis=0)
            lower = np.min(y, axis=0)
            res.append(np.mean(upper - lower))

        res = np.array(res)
        return np.argsort(res)[::-1]

    @staticmethod
    def _runModel(model, x):
        """

        Args:
            model: a list of dense and relu layers parsed from a nnet network
            x: the input of the model, numpy array

        Returns:

        """
        # assert x.shape == model[0]["input_shape"] or x.shape == (model[0]["input_shape"],)
        y = x
        for i in range(len(model)):
            layer = model[i]
            if layer["name"] == "dense":
                y = y @ layer["weight"] + layer["bias"]
            elif layer["name"] == "relu":
                y[y <= 0] = 0
        return y

    def __name__(self):
        return "OutputInfluenceScorer"
