\documentclass[aspectratio=169, dvipsnames, 12pt]{beamer}
\include{preamble_beamer}
\author{Julien Girard-Satabin (CEA LIST):
\href{mailto:julien.girard2@cea.fr}{julien.girard2@cea.fr}}
\title{Introduction to interpretable AI}
\subtitle{SETI Master 2024}
\date{\today}
\usepackage{appendixnumberbeamer}
\begin{document}

\begin{frame}[plain]{}
	\maketitle
\end{frame}

\section{Preliminaries}

\begin{frame}{You and I}
	\begin{figure}
		\centering
		\begin{subfigure}[t]{0.45\textwidth}
			\centering
			Myself:
			\begin{enumerate}
				\item researcher at CEA on formal methods for software safety
				      and security applied to machine learning;
				\item working on case-based reasoning and
				      out-of-distribution detection in industrial use cases;
				\item informed citizen;
			\end{enumerate}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.45\textwidth}
			\centering
			You:
			\begin{enumerate}
				\item M2 SETI master students;
				\item future practitionners of AI systems:
				      designer, developers, debuggers;
				\item informed citizens;
			\end{enumerate}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}{Hands on TP}
	\begin{enumerate}
		\item \lstinline{git clone git@git.frama-c.com:pub/seti_master.git}
		\item \lstinline{bash setup.sh}
        \item \lstinline{wait some time}
        \item \lstinline{wait launch.sh}
	\end{enumerate}

	This will download the required python environment and several other
	dependencies
\end{frame}

\begin{frame}{Definitions}
	\begin{defbox}{Explanation}
		"An explanation is a presentation of (aspects of) the reasoning, functioning and/or behavior of a machine learning model in human-understandable terms"~\cite{nauta2023anecdotal}

		"The \textbf{belief} (by the trustor) in the ability (of the trustee) to
		achieve \textbf{something}"
	\end{defbox}
\end{frame}

\begin{frame}{Explanation is a spectrum}
	Social science have quite a big corpus on what constitutes a good
	explanation~(\cite{miller2019explanation})?
	\begin{enumerate}
		\item \emph{contrastive}: why $P$ instead of $Q$?
		\item \emph{a social process}: $A$ explains $P$ to $B$
		\item \emph{more generic} (cover more facts), \emph{simpler} (quote less causes), and \emph{coherent} (related to previous knowledge) are more easily understood
	\end{enumerate}
\end{frame}

\begin{frame}{Why it matters}
	\begin{enumerate}
		\item debugging and audit
		\item refutability
		\item compliance with regulation (GDPR article 13.f~\cite{selbst2017meaningful})
	\end{enumerate}
\end{frame}


\section{Post-hoc explanations}

\begin{frame}{Notations}
	\begin{enumerate}
		\item samples $x \in \mathcal{X} \subseteq \mathbb{R}^{d}$ an input space, i$^{th}$ feature $x_i$
		\item an output $y \in \mathcal{Y} \subseteq \mathbb{R}^{p}$, the
		      i$^{th}$ feature $y_i$
		\item a program $f : \mathcal{X} \mapsto \mathcal{Y}$ trained on a $\mathcal{X}$
		      \begin{itemize}
			      \item we can usually decompose $f = h \circ g$
			      \item in the following, $h(x)$ is the output of an
			            intermediate layer for neural network
		      \end{itemize}
		\item $\nabla_x(y)$ is the gradient of $y$ at $x$
	\end{enumerate}
\end{frame}


% \begin{frame}{Linear regressions}
% 	\[
% 		y = \beta_0 + \beta_1x_1 + \beta_2x_2 + ... +
% 		\beta_nx_n + \varepsilon
% 	\]
% 	A feature will contribute to the decision by its linear coefficient:
% 	\[
% 		\beta_k = \frac{y - \sum_{i=1,i\neq k}^{i=n}\beta_i
% 			x_i}{x_k}
% 	\]
% \end{frame}

% \begin{frame}{LIME}
% 	Local Interpretable Model-agnostic
% 	Explanations (LIME)~\cite{ribeiro2016why}:

% 	\begin{enumerate}
% 		\item \emph{causal approach}: 	change $x_i$ to quantify their impact on $y$
% 		      \begin{itemize}
% 			      \item if no(sneeze) => no(flu), then sneeze is an important
% 			            feature
% 		      \end{itemize}
% 		\item  once relevant features are identified, train a \emph{surrogate model} that is easier to interpret
% 	\end{enumerate}

% \end{frame}

% \begin{frame}{LIME - cont.}
% 	\begin{minipage}{0.45\textwidth}
% 		\begin{figure}
% 			\begin{center}
% 				\includegraphics[height=0.6\textheight]{imgs/lime.png}
% 			\end{center}
% 			\caption*{}
% 		\end{figure}
% 	\end{minipage}
% 	\begin{minipage}{0.45\textwidth}
% 		\begin{figure}
% 			\begin{center}
% 				\includegraphics[height=0.3\textheight]{imgs/superpixel.jpeg}
% 				\hspace{3em}
% 				\includegraphics[height=0.3\textheight]{imgs/superpixel_bis.jpeg}
% 			\end{center}
% 		\end{figure}
% 	\end{minipage}

% 	The resulting surrogate model only explains \emph{one} prediction
% \end{frame}

% \begin{frame}{LIME pros and cons}
% 	\begin{minipage}{0.48\textwidth}
% 		Pros:
% 		\begin{enumerate}
% 			\item no need for the input data;
% 			\item no need to have access to the program;
% 		\end{enumerate}
% 	\end{minipage}
% 	\begin{minipage}{0.48\textwidth}
% 		Cons:
% 		\begin{enumerate}
% 			\item training process requires a notion of \emph{neighborhood}, which can be troublesome (images);
% 			\item no validity domain for the surrogate model;
% 		\end{enumerate}
% 	\end{minipage}
% \end{frame}

% \begin{frame}{Derivated approaches: Shapley values}
% 	\begin{enumerate}
% 		\item identify the mean-shift of each feature contribution
% 		      SHAP~\cite{lundberg2017unified} (Shapley values) to analyze ensemble
% 		      models
% 		\item gradually mask parts of the inputs (RISE~\cite{petsiuk2018rise})
% 	\end{enumerate}
% \end{frame}


\begin{frame}{Framework of feature attribution}
	\begin{figure}
		\begin{center}
			\includegraphics[width=0.7\textwidth]{./imgs/post-hoc.png}
		\end{center}
		\caption*{}
	\end{figure}
	core idea: computing $\nabla_x y$, identify the most relevant outputs on
	$\mathcal{Y}$ and project back on $\mathcal{X}$
\end{frame}

\begin{frame}{Some caveats}
	\begin{enumerate}
		\item gradient based approaches may not capture variations
		      \begin{itemize}
			      \item given $f(x) = 1-ReLu(1-x)$, $\nabla_0 f$ and $\nabla_2 f$ have the same value
		      \end{itemize}
		\item strong, local variations without any regularization scheme
	\end{enumerate}
\end{frame}

\begin{frame}{Smoothgrad}
	SMOOTHGRAD~\cite{smilkov2017smoothgrads} $\nabla_{x^{*}}(y)$ where $x^{*}$ is a gaussian neighborhood of $x$

	\[
		\nabla_{x^{*}}(y) \approx \frac{1}{n}\sum_0^{n}\nabla_xf(x+\mathcal{N}(0,\sigma))
	\]
\end{frame}

\begin{frame}{Integrated gradients}
	Gradient on the line between $x$ and a baseline image
	$x^{'}$~\cite{sundararajan2017axiomatic}

	\[
		IG_i = (x_i - x^{'}_i) \int_{\alpha=0}^{1} \nabla_{x_i}
		f(x^{'}+\alpha(x-x^{'}))d\alpha
	\]

	usually computed using Riemann approaches

	\[
		IG_i \approx (x_i - x^{'}_i) \sum_{k=0}^{m} \nabla_{x_i}
		f(x^{'}+\frac{m}{k}(x-x^{'}))*\frac{1}{m}
	\]
\end{frame}

\begin{frame}{Integrated gradients}
	\begin{figure}
		\begin{center}
			\includegraphics[height=0.7\textheight]{imgs/integrated_gradients.png}
		\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Wrapping up: empirical feature attribution approaches}
	\begin{enumerate}
		\item usually only require gradient computation access;
		\item provide attributions on the input space, but no direct exposition of the underlying decision process;
		\item heavily rely on the program internal representation;
		\item no validity domain;
	\end{enumerate}
\end{frame}

\section{Explanable by design programs}

\begin{frame}{Decision trees}
	\begin{figure}
		\begin{center}
			\includegraphics[height=0.6\textheight]{imgs/decision_tree.jpg}
		\end{center}
		\caption*{\tiny from Wikipedia~\url{https://en.wikipedia.org/wiki/Decision_tree_learning/}}
	\end{figure}
	Issue: the deeper the tree, the less amenable it is to understand its
	decision
\end{frame}

\begin{emptyslide}
	Integrating decision trees as the decision process for image classification
\end{emptyslide}

% \begin{frame}{Protoype based approaches - ProtoTrees}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[width=0.8\textwidth]{./imgs/prototree_training.png}
% 		\end{center}
% 		\caption*{}
% 	\end{figure}
% \end{frame}

\begin{frame}{Protoype based approaches - ProtoTrees}
	\begin{figure}
		\begin{center}
			\includegraphics[width=0.9\textwidth]{./imgs/prototree_inference.png}
		\end{center}
		\caption*{}
	\end{figure}
\end{frame}

\begin{frame}{Protoype based approaches - ProtoTrees}
	\begin{enumerate}
		\item learn "prototypes" $p$: part of the input set that
		      are deemed representative for the prediction;
		\item during inference, $M_i(x)$ are compared to $p_i$ using a similarity layer $S$;
	\end{enumerate}
\end{frame}

\begin{frame}{Protoype based approaches - Tackling tree complexity}
	ProtoTree have two hyperparameters that influence the decision tree:
	\begin{enumerate}
		\item the decision tree depth;
		\item the pruning threshold;
	\end{enumerate}
\end{frame}

\begin{frame}{Prototype based approaches - Caveat}
    \begin{enumerate}
        \item Still rely on the hypothesis that similarity in the feature space
            equals similarity in the input space;
        \item Need retraining models;
    \end{enumerate}
\end{frame}

% \section{Limitations}

% \begin{frame}{How to evaluate explanation methods?}
% 	Some criterion proposed by~\cite{nauta2023anecdotal}
% 	(Co12)
% 	\begin{enumerate}
% 		\item \emph{correction}
% 		\item \emph{cohérence} (implementation invariance)
% 		\item \emph{compactness} (size of the explanation)
% 		\item \emph{composability}
% 		\item \emph{controllability}
% 	\end{enumerate}
% \end{frame}

% \begin{frame}{How to evaluate explanation metrics?}
% 	See~\cite{xudarme2023stability,xudarme2023sanity}
% 	there is no "one size fits all" metric
% \end{frame}

% \begin{emptyslide}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[height=0.8\textheight]{imgs/explicabof_base.png}
% 			\caption*{From~\cite{molnar2022interpretable}}
% 		\end{center}
% 	\end{figure}
% \end{emptyslide}
% \begin{emptyslide}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[height=0.8\textheight]{imgs/explicabof.png}
% 			\caption*{From~\cite{molnar2022interpretable}}
% 		\end{center}
% 	\end{figure}
% \end{emptyslide}
% \begin{emptyslide}
% 	The network decision is ill-based. \emph{Why} is that? How to fix it?

% 	This explanation does not help to adjust our mental model on the program's
% 	behaviour, it is not a good one
% \end{emptyslide}

% \begin{frame}{Nuance}
% 	\emph{Extracting a causal chain and displaying it to a person is causal attribution, not (necessarily) an explanation}~\cite{miller2019explanation}.

% 	Attribution-based approaches are not enough to "fill the holes" for complex programs

% 	"\emph{How} the decision was taken" and "\emph{Why} the decision was taken" are two different questions
% \end{frame}

% \begin{frame}{Feeding our own biases}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[width=0.8\textwidth]{imgs/sanity_checks_data.png}
% 		\end{center}
% 		\caption*{\tiny From~\cite{tomsett2019sanity}}
% 	\end{figure}

% \end{frame}

% \begin{frame}{Feeding our own biases}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[width=0.8\textwidth]{imgs/sanity_checks.png}
% 		\end{center}
% 		\caption*{\tiny From~\cite{tomsett2019sanity}. The more on the right,
% 			the more random the network is.}
% 	\end{figure}
% \end{frame}


% \begin{frame}{Feeding our own biases}
% 	\begin{defbox}{Confirmation bias (Wikitionnaire)}
% 		(psychology) A cognitive bias towards confirmation of the hypothesis under study
% 	\end{defbox}

% 	A "nice" heatmap will confirm that the network works as expected, without being necessarily an accurate description of its inner working

% \end{frame}

% \begin{frame}{Explanations can be manipulated~\cite{dombrowski2019explanations}}
% 	\begin{figure}
% 		\begin{center}
% 			\includegraphics[height=0.8\textheight]{./imgs/manipulation.png}
% 		\end{center}
% 		\caption*{From~\cite{dombrowski2019explanations}}
% 	\end{figure}
% \end{frame}

% \section{The future?}

% \begin{frame}{Open questions}
% 	\begin{enumerate}
% 		\item validity of feature methods (for a variation on $f$? on $x$?)
% 		\item how to evaluate explanations and sort evaluation metrics?
% 		\item "social" explanation is yet to happen
% 	\end{enumerate}
% \end{frame}

% \begin{frame}{Our work, present and future}
% 	\begin{enumerate}
% 		\item case-based reasoning~\cite{xudarme2023particul}, out-of-distribution detection~\cite{xudarme2023contextualised}
% 		\item explainable by design approaches with a soon-to-come open source
% 		      library (CABRNET)
% 		\item formal explanation of AI
% 	\end{enumerate}

% 	Open to collaborations!
% \end{frame}

\appendix

\begin{frame}[allowframebreaks]{Bibliography}
	\printbibliography{}
\end{frame}


\end{document}
